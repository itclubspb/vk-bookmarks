var checkAuth = require(__dirname+'/authdb.js');
exports.auth = function(req, res, url, request, conf){
	var urlParsed = url.parse(req.url, true);
	var userName = "", userSurName = "", userToken = "", userBDate = "", userSex = "", userPhotoUrl = "";
	if(urlParsed.query.code)
	{
		var tokenUrl = "https://oauth.vk.com/access_token?client_id="+conf.get(conf.get("now")+"-vkId")+"&client_secret="+conf.get(conf.get("now")+"-vkKey")+"&code="+urlParsed.query.code+"&redirect_uri="+conf.get(conf.get("now")+"-domain")+"/vkAuth";
		request(tokenUrl, {json:true}, function (error, response, content) {
			{
				var userId = content.user_id;
				var userToken = content.access_token;
				getUserUrl = "https://api.vk.com/method/users.get?fields=bdate,sex,photo_100&user_id="+userId+"&v=5.28&access_token="+userToken;
				request(getUserUrl, {json:true}, function (error, response, content) {
					if (!error && response.statusCode == 200 && content && content.response && content.response[0])
					{
						content = content.response[0];
							userName = content.first_name;
							userSurName = content.last_name;
							userBDate = content.bdate;
							userSex = content.sex;
							userPhotoUrl = content.photo_100;
							var date = new Date();
							var today = date.getDate()+"."+date.getMonth()+"."+date.getFullYear();
							var infoUp = {
										id: userId,
										name: userName,
										lastname: userSurName,
										birthdate: userBDate,
										sex: userSex,
										photo: userPhotoUrl,
										lastseen: today
										};
							var infoReg = {
										id: userId,
										name: userName,
										lastname: userSurName,
										birthdate: userBDate,
										sex: userSex,
										photo: userPhotoUrl,
										registrated: today,
										lastseen: today,
										tags: [{}]
										};
						checkAuth(userId, req, res, infoUp, infoReg);
					}
				});
			}
		});
	}
}