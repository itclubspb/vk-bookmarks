var mongo = require('mongodb').MongoClient;
var conf = require('../config');

var checkAuth = function(id, req, res, dataUpdate, dataInsert){
	mongo.connect(conf.get("mongo")+""+conf.get("db"), function(err,db){
		if(err) throw err;
		var collection = db.collection('users');

		collection.find({id: id}).toArray(function(err,results){
			if(results.toString() != '')
			{
				collection.update({ id: id },{$set: dataUpdate}, function(err, results){db.close();});
				console.log("-------------------");
				console.log("Some user logged.\nId:", dataUpdate.id);
				console.log("-------------------");
			}
			else
			{
				collection.insert(dataInsert, function(err, docs){db.close();});
				console.log("-------------------");
				console.log("We've got new user.\nId:", dataInsert.id);
				console.log("-------------------");
			}
			req.session.user_id = dataUpdate.id;
			req.session.user_access = true;
			//req.session.user.token = token;
			req.session.user_name = dataUpdate.name;
			req.session.user_lastname = dataUpdate.lastname;
			res.redirect('/lastPosts');
		});
	});
}
module.exports = checkAuth;