var ejs = require('ejs');
var conf = require('../config');
var findHashtags = require('find-hashtags');
var getUrls = require('get-urls');
var mongo = require('mongodb').MongoClient;
exports.Posts = function(what, ID, req, res, tag){
	mongo.connect(conf.get("mongo")+""+conf.get("db"), mongoUse);
	function mongoUse(err,db){
		if(err) throw err;
		var collection = db.collection('users');
		var collposts = db.collection('posts');
			collection.find({id: ID}).toArray(function(err,results){
				if(results){
					userInfo = results[0];
					var postsCount = [0,0,0];//[0]-posts, [1]-archived, [2]-tag
					collposts.find({id: JSON.stringify(userInfo.id)}).toArray(function(err, results){
						for(var i=0; i<results.length; i++){
							if(results[i].option == "posts"){
								postsCount[0]++;
							}
							if(results[i].option == "archive"){
								postsCount[1]++;
							}
						}
						if(tag == "default"){var data = {id: JSON.stringify(userInfo.id), option: what};}
						if(tag == "tag"){var data = {id: JSON.stringify(userInfo.id), option: what, tags: req.url};}
						collposts.find(data).toArray(function(err, results){
							var isTherePosts = false;

							if(results[0]){
								isTherePosts = true;
								var posts = [{}];
								for(var i=0; i<results.length; i++) {
									if(i>0){posts.push({});}
									if(tag == "tag"){postsCount[2]++;}
									posts[i].tags = results[i].tags;
									posts[i].lastUse = results[i].lastUse;
									posts[i].date = unixTimeNormalize(results[i].post.response[0].date);
									posts[i].typeCol = [0,0,0,0,0,0];	//[0]-photo, [1]-audio, [2]-video, [3]-links, [4]-docs, [5]-album
									
									posts[i].owner = JSON.stringify(results[i].post.response[0].owner_id);	//ownerURL
									posts[i].owner = posts[i].owner[0]=="-" ? posts[i].owner.replace("-", "http://vk.com/club") : "http://vk.com/id"+posts[i].owner; //ownerURLNormalize
									posts[i].ownerId = JSON.stringify(results[i].post.response[0].owner_id);
									posts[i].postId = JSON.stringify(results[i].post.response[0].id);
									posts[i].avatar = results[i].ava;
									posts[i].author = results[i].author; // Post owner name 
									posts[i].text = results[i].post.response[0].text != undefined ? makePostText(i, results[i].post.response[0].text, posts[i].ownerId): undefined; // Post content
									
									var attachments = results[i].post.response[0].attachments;
									if(attachments)
									{
										posts[i].photo = [[]];
										posts[i].audio = [[]];
										posts[i].video = [[]];
										posts[i].links = [[]];
										posts[i].docs  = [[]];
										posts[i].album = [[]];
										for(var j=0; j<attachments.length; j++){
											if(attachments[j].type=="photo")
											{
												posts[i].photo.push([[]]);
												posts[i].photo[posts[i].typeCol[0]][0] = attachments[j].photo.photo_130;
												posts[i].photo[posts[i].typeCol[0]][1] = attachments[j].photo.photo_604;
												posts[i].photo[posts[i].typeCol[0]][2] = attachments[j].photo.photo_807;
												posts[i].photo[posts[i].typeCol[0]][3] = attachments[j].photo.width;
												posts[i].photo[posts[i].typeCol[0]][4] = attachments[j].photo.height;

												posts[i].typeCol[0]++;
											}
											if(attachments[j].type=="audio")
											{
												posts[i].audio.push([[]]);
												posts[i].audio[posts[i].typeCol[1]][0] = attachments[j].audio.artist;
												posts[i].audio[posts[i].typeCol[1]][1] = attachments[j].audio.title;
												posts[i].audio[posts[i].typeCol[1]][2] = attachments[j].audio.url;
												if((attachments[j].audio.duration-(60*parseInt(attachments[j].audio.duration/60)))<10) var sec = 0+""+(attachments[j].audio.duration-(60*parseInt(attachments[j].audio.duration/60)));
												else var sec = (attachments[j].audio.duration-(60*parseInt(attachments[j].audio.duration/60)));
												posts[i].audio[posts[i].typeCol[1]][3] = parseInt(attachments[j].audio.duration/60)+":"+sec;

												posts[i].typeCol[1]++;
											}
											if(attachments[j].type=="video")
											{
												posts[i].video.push([[]]);
												posts[i].video[posts[i].typeCol[2]][0] = attachments[j].video.photo_320;
												posts[i].video[posts[i].typeCol[2]][1] = attachments[j].video.title;
												var viDuration = attachments[j].video.duration;
												var hours = 0, mins = 0, sec = 0;
												if(viDuration>=3600)
												{
													hours = parseInt(viDuration/3600);
													viDuration -= hours*3600;
													hours += ":";
												}
												else{
													hours = "";
												}
												if(viDuration>=60)
												{
													var mins = parseInt(viDuration/60);
													viDuration -= mins*60;
													if(hours!="" && mins<10) mins="0"+mins;
													mins += ":";
												}
												else{
													mins = "0:";
												}
												var sec = viDuration;
												if(mins!="" && sec<10) sec = "0"+sec;
												posts[i].video[posts[i].typeCol[2]][2] = hours+mins+sec;
												
												posts[i].typeCol[2]++;
											}
											if(attachments[j].type=="link")
											{
												posts[i].links.push([[]]);
												posts[i].links[posts[i].typeCol[3]][0] = attachments[j].link.url;
												posts[i].links[posts[i].typeCol[3]][1] = attachments[j].link.title;
												posts[i].links[posts[i].typeCol[3]][2] = attachments[j].link.description;
												posts[i].links[posts[i].typeCol[3]][3] = attachments[j].link.image_src;
												
												posts[i].typeCol[3]++;
											}
											if(attachments[j].type=="doc")
											{
												posts[i].docs.push([[]]);
												posts[i].docs[posts[i].typeCol[4]][0]  = attachments[j].doc.url;
												posts[i].docs[posts[i].typeCol[4]][1]  = attachments[j].doc.size>1024*1024 ? parseInt(attachments[j].doc.size/(1024*1024))+"Mb": attachments[j].doc.size>1024 ? parseInt(attachments[j].doc.size/1024)+"Kb": attachments[j].doc.size/1024+"b";
												posts[i].docs[posts[i].typeCol[4]][2]  = attachments[j].doc.photo_130;
												posts[i].docs[posts[i].typeCol[4]][3]  = attachments[j].doc.title;
												posts[i].docs[posts[i].typeCol[4]][4]  = attachments[j].doc.ext;
												
												posts[i].typeCol[4]++;
											}
											if(attachments[j].type=="album")
											{
												posts[i].album.push([[]]);
												posts[i].album[posts[i].typeCol[5]][0]  = attachments[j].album.thumb.photo_604;
												posts[i].album[posts[i].typeCol[5]][1]  = attachments[j].album.thumb.width;
												posts[i].album[posts[i].typeCol[5]][2]  = attachments[j].album.thumb.height;
												posts[i].album[posts[i].typeCol[5]][3]  = attachments[j].album.title;
												posts[i].album[posts[i].typeCol[5]][4]  = attachments[j].album.size;
												posts[i].album[posts[i].typeCol[5]][5]  = attachments[j].album.thumb.album_id;
												posts[i].album[posts[i].typeCol[5]][6]  = attachments[j].album.thumb.owner_id;
												
												posts[i].typeCol[5]++;
											}
										}
									}
								}
								
							}
							var data = {
										category: what,
										tag: [tag, req.url],
										postsCount: postsCount,
										user: userInfo,
										isTherePosts: isTherePosts,
										posts: sortBub(posts)
									};
							res.render('Vk/index.ejs', data);
							//res.end(JSON.stringify(data));
							db.close();
						});
					});
				}
		});
	}
}
function sortBub(a){
	if(a){
		for(var i=0; i<a.length; i++){
			for(var j=0; j<a.length-1-i; j++){
				if(a[j].lastUse>a[j+1].lastUse){
					var temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
			}
		}
		return a;
	}
}
function makePostText(iter, text, id){
	var cutted = false;
	var urls = getUrls(text);
	for(var k=0; k<urls.length; k++){
		var UrlToLink = urls[k].length>60 ? '<a class = "linked" href = "' : '<a href = "';
		if(urls[k].search("http://")==0 || urls[k].search("https://")==0){
			UrlToLink += urls[k]+"";
		}
		else{
			UrlToLink += 'http://'+urls[k];
		}
		UrlToLink += '" target = "_blank">'+decodeURI(urls[k])+'</a>';
		text = text.replace(urls[k], UrlToLink);
	}
	if(text.length>600){
		if(text.indexOf("\n",250)<400 && text.indexOf("\n",250)!=-1){
			text = text.slice(0, text.indexOf("\n",250))
					+"\n<div class = 'openMe' onclick = 'showPost("+iter+", this)'>Показать полностью..</div><span class = 'closedSpan closedSpan"+iter+"'>"
					+text.slice(text.indexOf("\n",250)+1, text.length);
			cutted = true;
		}
		if(!cutted){
			if(text.indexOf(" ",300)<400 && text.indexOf(" ",300)!=-1){
				text = text.slice(0, text.indexOf(" ",300))
					+"<div class = 'openMe' onclick = 'showPost("+iter+", this)'>Показать полностью..</div><span style = 'margin-left: 5px;' class = 'closedSpan closedSpan"+iter+"'>"
					+text.slice(text.indexOf(" ",300)+1, text.length);
				cutted = true;
			}
		}
	}
	var tempL = /(\[)(\w+)(\|)(.+)(\])/ig;
	text = text.replace(tempL, "<a href = 'http://vk.com/$2' target = '_blank'>$4</a>");
	var re = /(#)([а-яa-z\+_0-9]+)(@\w+)?/ig;
	text = text.replace(re, '<a href="http://vk.com/wall'+id+'?q=%23$2" target = "_blank">#$2$3</a>');
	// re =  /\B(#)(\w*[a-zа-я]+\w*)\B/ig;
	// text = text.replace(re, '<a href="http://vk.com/feed?q=%23$2&section=search" target = "_blank">#$2</a>');
	text =  text.replace(new RegExp("\n",'g'), "<br/>\n");
	var splitted = text.split("\n");
	if(!cutted && splitted.length>7){
		splitted[4] += "<div class = 'openMe' onclick = 'showPost("+iter+", this)'>Показать полностью..</div><span class = 'closedSpan closedSpan"+iter+"'>"
	}
	return splitted;
}


function unixTimeNormalize(unix){
	var uniX = new Date(unix*1000);
	var months = ['Янв.','Фев.','Мар.','Апр.','Май.','Июн.','Июл.','Авг.','Сен.','Окт.','Ноя.','Дек.'];
	var year = uniX.getFullYear();
	var month = months[uniX.getMonth()];
	var date = uniX.getDate();
	var hour = uniX.getHours();
	hour = hour.toString().length<2 ? "0"+hour : hour;
	var min = uniX.getMinutes();
	min = min.toString().length<2 ? "0"+min : min;
	var sec = uniX.getSeconds();
	sec = sec.toString().length<2 ? "0"+sec : sec;
	var mDate = {
		date: date+' '+month+' '+year,
		time: hour+':'+min+':'+sec
	}
	return mDate;	
}