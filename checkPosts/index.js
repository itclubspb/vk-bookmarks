var mongo = require('mongodb').MongoClient;
var conf = require('../config');
var checkPosts = function(urlParsed, res){
	var userId = urlParsed.query.userId;
	var fromId = urlParsed.query.fromId;
	var postId = urlParsed.query.postId;

	mongo.connect(conf.get("mongo")+""+conf.get("db"), mongoUse);

	function mongoUse(err,db){
		if(err) throw err;
		var posts = db.collection('posts');
		posts.find({ownerId: fromId, id: userId, postId:postId, option: "posts"}).toArray(function(err,results){
			var isIt = {isIt: false};
			if(results[0]){
				isIt.isIt = true;
			}
			db.close();
			res.writeHead(200, {"Content-Type": "application/json",
									 'Access-Control-Allow-Origin' : '*'});
			res.end(JSON.stringify(isIt));
		});
	}
}
module.exports = checkPosts;