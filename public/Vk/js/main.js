function photosPlacer(prefix, width, height, classNum, photosNum){
	var rel = width/height;
	if(photosNum === 1){
		$("."+prefix+"postImg"+classNum).css("width", "430px");
		$("."+prefix+"postImg"+classNum).css("height", 430/rel+"px");
	}
	if(rel<=0.9){
		if(photosNum === 1){
			$("."+prefix+"postImg"+classNum).css("width", "250px");
			$("."+prefix+"postImg"+classNum).css("height", 250/rel+"px");
		}
	}
	if(photosNum === 2){
		$("."+prefix+"postImg"+classNum).css("width", "250px");
		$("."+prefix+"postImg"+classNum).css("height", 250/rel+"px");
	}
	if(photosNum % 3 == 0){
		$("."+prefix+"postImg"+classNum).css("width", "163px");
		$("."+prefix+"postImg"+classNum).css("height", 163/rel+"px");
	}
	if(photosNum === 4 || photosNum === 8){
		$("."+prefix+"postImg"+classNum).css("width", "120px");
		$("."+prefix+"postImg"+classNum).css("height", 120/rel+"px");
	}
	if(photosNum === 5){
		for(var z=0; z<photosNum; z++){
			if(z<2){
				$("#"+prefix+"postImg"+classNum+z).css("width", "249px");
				$("#"+prefix+"postImg"+classNum+z).css("height", 249/rel+"px");
			}
			else{
				$("#"+prefix+"postImg"+classNum+z).css("width", "163px");
				$("#"+prefix+"postImg"+classNum+z).css("height", 163/rel+"px");
			}
		}
	}
	if(photosNum === 7 || photosNum === 10)
	{
		for(var z=0; z<photosNum; z++){
			if(z<photosNum-4){
				$("#"+prefix+"postImg"+classNum+z).css("width", "163px");
				$("#"+prefix+"postImg"+classNum+z).css("height", 163/rel+"px");
			}
			else{
				$("#"+prefix+"postImg"+classNum+z).css("width", "120px");
				$("#"+prefix+"postImg"+classNum+z).css("height", 120/rel+"px");
			}
		}
	}
}
var lastScroll = 0;
$(window).scroll(function(){
	if($("#tagTable").css("display")=="block"){$("#tagTable").css("position", "absolute");}
	if ( $(this).scrollTop() > 250 ){
		$("#go_top").css("display", "block");
		$("#go_top").css("opacity", ".3");
		$("#go_top_text").html("Наверх");
		$("#go_top_img").css("background", "url(../Vk/images/toplink.gif?3) no-repeat left 3px");
		lastScroll = 0;
	}
	if ( $(this).scrollTop() > 270 ){
		$("#go_top").css("display", "block");
		$("#go_top").css("opacity", ".5");
	}
	if ( $(this).scrollTop() > 330 ){
		$("#go_top").css("display", "block");
		$("#go_top").css("opacity", ".7");
	}
	if ( $(this).scrollTop() <= 330 ){
		$("#go_top").css("display", "block");
		$("#go_top").css("opacity", ".5");
	}
	if ( $(this).scrollTop() <= 270 ){
		$("#go_top").css("display", "block");
		$("#go_top").css("opacity", ".3");
	}
	if ( $(this).scrollTop() < 250){
		if(lastScroll === 0) $("#go_top").css("display", "none");
		if(lastScroll !== 0){
			$("#go_top").css("display", "block");
			$("#go_top").css("display", "block");
			$("#go_top_text").html("");
			$("#go_top_img").css("background", "url(../Vk/images/toplink.gif?3) no-repeat left -7px");
		}
		
	}
});
var showPost = function(id,b){
	//$(".closedSpan").css("display", "none");
	//$(".openMe").css("display", "block");         Если убрать комменты, то при каждом "Показать полностью" придыдущий будет закрываться.
	$(".closedSpan"+id).css("display", "inline");
	$(b).css("display", "none");
};
var scroll = function(nowScroll){
	if(nowScroll>250){
		lastScroll = nowScroll;
		$(window).scrollTop(0);
	}
	if(lastScroll && nowScroll<250){
		$(window).scrollTop(lastScroll);
		
	}
};

function deletePost(a, owner, post, id) { 
	jQuery.ajax({ 
		url: "/deletePost",
		type: "GET", 
		dataType: "html",
		data: "a="+owner+"&b="+post+"&c="+id+"&d="+window.location.pathname.toString(),
		success: function(response){}, 
		error: function(response) {} 
	});
	$(a).offsetParent().css("display", "none");
}
function backPost(a, owner, post, id) { 
	jQuery.ajax({ 
		url: "/backPost",
		type: "GET", 
		dataType: "html",
		data: "a="+owner+"&b="+post+"&c="+id+"&d="+window.location.pathname.toString(),
		success: function(response){}, 
		error: function(response) {} 
	});
	$(a).offsetParent().css("display", "none");
}
function makeNewTag(value, elem){
	if(value.trim())
	{
		jQuery.ajax({ 
			url: "/newTag",
			type: "GET", 
			dataType: "html",
			data: "tag="+value,
			success: function(response){
				if(response != "found"){
					$("#upon").before( "<li><a href='"+response+"'>"+value+"</a></li>" );
					$(elem).val("");
				}
				else{
					$("#upon").addClass("tagFailed");
					$(elem).fadeOut(500).val("").fadeIn();
				}
			}, 
			error: function(response) {
				alert("Ошибка сервера :(");
			}
		});
	}
	else{
		$(elem).val("");
	}
}

function addTag(a, owner, post, tags, allTags, id){
	if($("#tagTable").css("display") == "block"){
		$("#tagTable").css("display", "none");
		if(!($("#tagTable").hasClass(id))){
			addTag(a, owner, post, tags, allTags, id);
		}
	}
	else{
		allTags = JSON.parse(allTags);
		tags = JSON.parse(tags);
		$("#tagTable").removeClass();
		$("#tagTable").addClass(id);
		$("#tagTable").css("display", "block");
		$("#tagTable").css("top", (window.event.pageY-10)+"px");
		$("#tagTable").css("left", (window.event.pageX-185)+"px");
		console.log(window.event.pageX, window.event.pageY);
		var makeContent = "<ul id = 'tagsList'>";
		for(var i=0; i<allTags.length; i++){
			var flag = false;
			for(var j=0; j<tags.length; j++){
				if(tags[j] == allTags[i].href){
					makeContent+="<li class = 'tagUsed tagItem' onclick = \"changeTagState(this); useTagToPost('"+owner+"','"+post+"','"+allTags[i].href+"')\"><div class = 'tagName'>"+allTags[i].name+"</div><div class = 'tagImg'></div></li>";
					flag = true;
					break;
				}
			}
			if(!flag){makeContent+="<li class = 'tagNotUsed tagItem' onclick = \"changeTagState(this); useTagToPost('"+owner+"','"+post+"','"+allTags[i].href+"')\"><div class = 'tagName'>"+allTags[i].name+"</div><div class = 'tagImg'></div></li>";}
		}
			console.log(tags.length);	
		makeContent += "</ul>";
		$("#tagTable").html(makeContent);
	}
}
$(document).click(function(event) {
    if (!$(event.target).closest(".tagItem, #tagTable, .addTag, #tagsList").length) {
		$("#tagTable").css("display", "none");
    }
});
function useTagToPost(a,b,c){
	jQuery.ajax({ 
			url: "/changeTagState",
			type: "GET", 
			dataType: "html",
			data: "owner="+a+"&post="+b+"&tag="+c,
			success: function(response){
				console.log("success!!");
				console.log(response);
			}, 
			error: function(response) {
				alert("Ошибка сервера :(");
			}
		});
}
function changeTagState(a){
	if($(a).hasClass("tagUsed")){
		$(a).removeClass("tagUsed")
		$(a).addClass("tagNotUsed");
	}
	else{
		$(a).removeClass("tagNotUsed")
		$(a).addClass("tagUsed");
	}
}