function setDefaults(id){
	document.getElementById("audioController"+id).style.backgroundPosition = "top";
	document.getElementById("audio"+id).style.height = "28px";
	document.getElementById("audio"+id).style.marginBottom = "16px";
	audio.pause();
	audio.currentTime = 0;
	return true;
}
var audio;
var duration;
var startDuration;
var volume;
function audioController(id,count,local){
	if(audio && audio.id.replace(/\D+/g,"")==id){
		PlayPause(id,count,local);
	}
	if(audio && (audio.id && audio.id.replace(/\D+/g,"")!=id))
	{
		var setDef = setDefaults(audio.id.replace(/\D+/g,""));
		clearInterval(startDuration);
		if(setDef)
		{
			audio = document.getElementById("audioPlayer"+id);
			duration = document.getElementById("audioDuration"+id);
			duration.max = audio.duration;
			volume = document.getElementById("audioVolume"+id);
			audio.volume = $(".audioVolume").val()/100;
			PlayPause(id,count,local);
		}
	}
	if(!audio)
	{
		audio = document.getElementById("audioPlayer"+id);
		duration = document.getElementById("audioDuration"+id);
		duration.min = 0;
		duration.max = audio.duration;
		volume = document.getElementById("audioVolume"+id);
		volume.value = $("#audioVolume"+id).val();
		if(audio.id && audio.id.replace(/\D+/g,"")==id)
		{
			PlayPause(id,count,local);
		}
	}
}

function PlayPause(id,count,local){
	document.getElementById("audio"+id).style.height = "44px";
	document.getElementById("audio"+id).style.marginBottom = "auto";
	if(audio.paused){
		audio.play();
		document.getElementById("audioController"+id).style.backgroundPosition = "bottom";
		startDuration = setInterval(function(){
										duration.value = audio.currentTime;
										document.getElementById("audioDurationNow"+id).style.width = (duration.value*100/audio.duration)+"%";
										if(audio.currentTime == audio.duration){nextPlay(id,count,document.getElementsByClassName("audio"+count).length,local);}
										}, 1000/66);
	}
	else{
		audio.pause();
		document.getElementById("audioController"+id).style.backgroundPosition = "top";
		clearInterval(startDuration);
	}
}
function nextPlay(id, count, playCount, local){
	console.log("Now It's", id, count, playCount, local);
	console.log(parseInt(playCount)==parseInt(local)+1);
	clearInterval(startDuration);
	if(parseInt(playCount)==parseInt(local)+1){
		console.log("here");
		local=0;
		nextId = count+""+local;
	}
	else{
		local++;
		nextId = count+""+local;
	}
	console.log("NextIds: ",nextId,count,local);
	audioController(nextId,count,local);
}
function clearAnimateRange(){
	clearInterval(startDuration);
}
function movedRange(id, count, local){
	audio.currentTime = duration.value;
	if(!audio.paused){audio.pause();PlayPause(id, count, local);}
	document.getElementById("audioDurationNow"+id).style.width = duration.value*100/audio.duration+"%";
}

function changeVolume(id){
	audio.volume = volume.value/100;
	$(".audioVolume").val(volume.value);
	$(".audioVolumeNow").css("width", volume.value+"%");
	document.getElementById("audioVolumeNow"+id).style.width = volume.value+"%";
}