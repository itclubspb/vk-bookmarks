var mongo = require('mongodb').MongoClient;
var conf = require('../config');
exports.change = function(urlParsed, res, userId){
	var owner = JSON.stringify(urlParsed.query.owner);
	var post = JSON.stringify(urlParsed.query.post);
	var tag = urlParsed.query.tag;
	mongo.connect(conf.get("mongo")+""+conf.get("db"), function(err,db){
		if(err) throw err;
		var collposts = db.collection('posts');
		collposts.find({id: JSON.stringify(userId), postId: urlParsed.query.post, ownerId:urlParsed.query.owner}).toArray(function(err,results){
				if(results[0]){
					if(results[0].tags){
						var newTags = results[0].tags;
						var flag=false;
						for(var i=0; i<newTags.length; i++){
							if(newTags[i]==tag){
								flag = true;
								if(newTags.length==1){newTags=[];}
								else{newTags.splice(i,i);}
								break;
							}
						}
						if(!flag){
							newTags.push(tag);
						}
					}else{
						var newTags = [tag];
					}
					collposts.update({id: JSON.stringify(userId), postId: urlParsed.query.post, ownerId:urlParsed.query.owner},{$set: {tags: newTags}}, function(err, results){
						db.close();
						res.end("goog");
					});
				}
				else{
					res.end("bad");
					db.close();
				}
		});
	});
}

exports.add = function(urlParsed, res, userId){
	var value = urlParsed.query.tag;

	mongo.connect(conf.get("mongo")+""+conf.get("db"), function(err,db){
		if(err) throw err;
		var collusers = db.collection('users');

		collusers.find({id: userId}).toArray(function(err,results){
			if(results[0]){
				var found = false;
				if(results[0].tags)
				{
					for(var i=0; i<results[0].tags.length; i++){
						if(results[0].tags[i].name == value){
							db.close();
							res.end("found");
							var found = true;
							break;
						}
					}
				}
				
				if(!found){
					if(results[0].tags){
						var tags = results[0].tags;
						if(results[0].tags[0].name == undefined){
							var tags = [{
									name: value,
									href: "/tag_1",
									visible: true
							}];
						}
						else{
							tags.push({
										name: value,
										href: "/tag_"+(results[0].tags.length+1),
										visible: true
							});
						}
					}
					else{
						var tags = [{
									name: value,
									href: "/tag_1",
									visible: true
						}];
					}
					collusers.update({ id: userId },{$set: {tags: tags}}, function(err, results){
						db.close();
						res.end(tags[tags.length-1].href);
					});
				}
			}
		});
	});
}