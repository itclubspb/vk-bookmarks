var express = require('express'),
	app = express();
var conf = require('./config');
var io = require('socket.io').listen(app.listen(conf.get('port')));
//----------------//
var show = require('./showPosts');
var archive = require('./archivePosts');
var tags = require('./tags');
var checkPosts = require('./checkPosts');
var fs = require('fs');
var url = require('url');
var ejs = require('ejs');
var vkAuth = require('./auth');
var addPost = require('./addPosts');
var request = require('request');
var path = require('path');
//----------------//
app.engine(conf.get('tpl'), require('ejs-locals'));
app.set('views', path.join(__dirname, conf.get('app-view')));
app.set('view engine', conf.get('tpl'));
app.use(express.static(path.join(__dirname, conf.get('app-static'))));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.cookieParser());
app.use(express.session({secret: 'SOLDSOLDSOLDSOLDSOLDSOLDSOLDSOLDSOLDSOLD', key: 'express.sid'}));
app.use(app.router);
//----------------//
app.get('/', function(req, res){
	var redirect = conf.get(conf.get("now")+"-domain");
	var vkApiId = conf.get(conf.get("now")+"-vkId");
	req.session.user_access && res.redirect('https://oauth.vk.com/authorize?client_id='+vkApiId+'&scope=wall&redirect_uri='+redirect+'/vkAuth&response_type=code&v=5.28');
	!req.session.user_access && res.render("landing.ejs", {redirect: redirect, vkApiId: vkApiId});
});
//----------------//
app.get('/vkAuth', function(req, res){
	vkAuth.auth(req,res,url,request,conf);
});
//----------------//
app.get('/lastPosts', function(req, res){
	req.session.user_access && show.Posts("posts", req.session.user_id, req, res, "default");
	!req.session.user_access && res.redirect('/');
});
app.get('/archive', function(req, res){
	req.session.user_access && show.Posts("archive", req.session.user_id, req, res, "default");
	!req.session.user_access && res.redirect('/');
});
//----------------//
app.get('/deletePost*', function(req, res){
	archive(url.parse(req.url, true), res, "archive");
});
app.get('/backPost*', function(req, res){
	archive(url.parse(req.url, true), res, "posts");
});
app.get('/newTag*', function(req, res){
	tags.add(url.parse(req.url, true), res, req.session.user_id);
});
app.get('/changeTagState*', function(req, res){
	tags.change(url.parse(req.url, true), res, req.session.user_id);
});
app.get('/tag_*', function(req, res){
	req.session.user_access && show.Posts("posts", req.session.user_id, req, res, "tag");
	!req.session.user_access && res.redirect('/');
});
//----------------//
app.get('/exit', function(req, res){
	req.session.user_access = false;
	res.redirect('/');
});
//----------------//
app.get('/newPost?*',function(req, res){
	var urlParsed = url.parse(req.url, true);
	addPost.add(urlParsed, request, res);
});
//----------------//
app.get('/checkPosts*',function(req, res){
	var urlParsed = url.parse(req.url, true);
	checkPosts(urlParsed, res);
});
app.get('team', function(req, res){
	
});
app.get('*', function(req, res){
	res.end("404");
});