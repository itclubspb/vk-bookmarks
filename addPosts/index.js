var mongo = require('mongodb').MongoClient;
var conf = require('../config');
exports.add = function(urlParsed, request, res){
	var userId = urlParsed.query.userId;
	var fromId = urlParsed.query.fromId;
	var postId = urlParsed.query.postId;
	var postAuthor = decodeURI(urlParsed.query.postAuthor);
	var postImg = urlParsed.query.postImage;
	var requestUrl = 'https://api.vk.com/method/wall.getById?posts='+fromId+'_'+postId+'&user_id='+userId+'&v=5.28';
	
	request(requestUrl, {json:true}, function (error, response, content) {
		if(content.response[0]!=undefined)
		{
			mongo.connect(conf.get("mongo")+""+conf.get("db"), function(err,db){
				if(err) throw err;
				var collection = db.collection('posts');
				var data = {
								id: userId,
								author: postAuthor,
								ava: postImg,
								ownerId: fromId,
								postId: postId,
								option: "posts",
								lastUse: Date.now(),
								tags: [],
								post: content
							}
				collection.find({id: userId,ownerId: fromId,postId: postId}).toArray(function(err,results){
					if(results[0]){
						collection.update({id: userId,ownerId: fromId,postId: postId},{ $set: {option: "posts", post : content, lastUse: Date.now()}}, function(err, result) {
							db.close();
							res.writeHead(200, {
												 'Content-Type': 'text/html',
												 'Access-Control-Allow-Origin' : '*'
												});
							res.end("thanks :)");
						});
					}
					else{
						collection.insert(data, function(err, docs){
							db.close();
							res.writeHead(200, {
												 'Content-Type': 'text/html',
												 'Access-Control-Allow-Origin' : '*'
												});
							res.end("thanks :)");
						});
					}
				});
			});
		}
	});
}