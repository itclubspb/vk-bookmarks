var conf = require('../config');
var mongo = require('mongodb').MongoClient;

var postArchive = function(urlParsed, res, to){
	var owner = urlParsed.query.a,
		post = urlParsed.query.b,
		userId = urlParsed.query.c,
		where = urlParsed.query.d;

	mongo.connect(conf.get("mongo")+""+conf.get("db"), mongoUse);
	function mongoUse(err,db){
		if(err) throw err;
		var posts = db.collection('posts');
		if(to == "archive")
		{
			if(where === "/lastPosts"){
				posts.update({id: userId, ownerId: owner,postId: post},{ $set: {option : "archive", lastUse: Date.now()}}, function(err, result) {
					db.close();
					res.end();
				});
			}
			if(where === "/archive"){
				posts.update({id: userId,ownerId: owner,postId: post},{ $set: {option : "deleted", lastUse: Date.now()}}, function(err, result) {
					db.close();
					res.end();
				});
			}
		}
		else{
			if(to == "posts"){
				posts.update({id: userId, ownerId: owner,postId: post},{ $set: {option : "posts", lastUse: Date.now()}}, function(err, result) {
					db.close();
					res.end();
				});
			}
		}
	}
}
module.exports = postArchive;